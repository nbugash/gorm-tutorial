# GORM-tutorial

A simple tutorial taken from Pluralsight on how to use GORM - an object-relational mapper fro GO

## Table of Content
- [Introduction](#introduction)
  * [Overview of GORM's features](#overview-of-gorm-s-features)
  * [Where to find Documentation](#where-to-find-documentation)
  * [Intro to Demo Scenario](#intro-to-demo-scenario)
  * [Establishing a Database connection](#establishing-a-database-connection)
  * [Creating Tables](#creating-tables)
  * [Creating Records](#creating-records)
  * [Querying for Records](#querying-for-records)
  * [Updating Records](#updating-records)
  * [Deleting Records](#deleting-records)
- [Defining Schemas](#defining-schemas)
  * [Defining Entities](#defining-entities)
  * [Scoping](#scoping)
  * [Defining Tables](#defining-tables)
  * [Basic Entity Creation](#basic-entity-creation)
  * [Customizing Field Types and Sizes](#customizing-field-types-and-sizes)
  * [Auto-incrementing fields](#auto-incrementing-fields)
  * [Transient fields](#transient-fields)
  * [Unique fields](#unique-fields)
  * [Preventing Nulls and Providing Default Values](#preventing-nulls-and-providing-default-values)
  * [Primary Key fields](#primary-key-fields)
  * [Controlling Columns Names](#controlling-columns-names)
  * [Embedding Child objects](#embedding-child-objects)
  * [Working with Indexes](#working-with-indexes)
- [Working with Relationships](#working-with-relationships)
  * [One-to-One relationships](#one-to-one-relationships)
  * [Foreign key contraints](#foreign-key-contraints)
  * [One-to-many relationships](#one-to-many-relationships)
  * [Many-to-many relationships](#many-to-many-relationships)
  * [Polymorphism](#polymorphism)
  * [Association API](#association-api)
- [Creating, Updating, and Deleting Records](#creating--updating--and-deleting-records)
  * [Creating Records](#creating-records-1)
  * [Creating Records with Children](#creating-records-with-children)
  * [Updating Records](#updating-records-1)
  * [Updating Records with Children](#updating-records-with-children)
  * [Batch Updates](#batch-updates)
  * [Deleting Records](#deleting-records-1)
  * [Transactions](#transactions)
- [Querying the Database](#querying-the-database)
  * [Retrieving Single Records](#retrieving-single-records)
  * [Retrieving Record Sets](#retrieving-record-sets)
  * [Where Clause](#where-clause)
  * [Preloading Child Object](#preloading-child-object)
  * [Limits, Offsets, and Ordering Records](#limits--offsets--and-ordering-records)
  * [Selecting Data subsets](#selecting-data-subsets)
  * [Using Attrs and Assign to Provide Default values](#using-attrs-and-assign-to-provide-default-values)
  * [Creating projections with Joins](#creating-projections-with-joins)
  * [Working with Raw Result Rows](#working-with-raw-result-rows)
  * [Creating Aggregations with Group and Having](#creating-aggregations-with-group-and-having)
  * [Using Raw SQL](#using-raw-sql)
- [Modifying Schema](#modifying-schema)
  * [Creating new entities](#creating-new-entities)
  * [Adding fields to an Entity](#adding-fields-to-an-entity)
  * [Adding indexes](#adding-indexes)
  * [Removing Entities](#removing-entities)
  * [Modifying a field's type](#modifying-a-field-s-type)
  * [Removing a field](#removing-a-field)
- [Advance topic](#advance-topic)
  * [Callbacks](#callbacks)
  * [Scopes](#scopes)
  * [Customizing the Logger](#customizing-the-logger)

## Introduction
### Overview of GORM's features
* Basic CRUD operations
* Schema management - such as creating keys, foreign key contraints, etc
* Keeping operation in a transactions - if an operation fails, it will rollback as if none of the operations were executed
* Migration
* Event Hooks - allows to call functions before or after an event gets trigger
* Chainable API
* Logger - used to inspect what is going on with the application

Supported Database
* Postgresql
* MySql
* SQLite
* Foundation

[Back](#table-of-content)

### Where to find Documentation
The full documentation can be found [here](https://github.com/go-gorm/gorm)

[Back](#table-of-content)

### Intro to Demo Scenario

[Back](#table-of-content)


### Establishing a Database connection
First we need to get GORM from github

        go get -u github.com/go-gorm/gorm

Next we need to get the database driver. Since we're going to use `Postgresql`, we need to get its driver

        go get -u github.com/lib/pq

```go
package main

import (
    "github.com/go-gorm/gorm"
    _ "github.com/lib/pq"
)

func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()
    
    err = dbase.Ping()
    if err != nil {
        panic(err.Error())
    }

    fmt.Println("Connection to database established")
}
```

[Back](#table-of-content)


### Creating Tables

```go
type User struct {
    ID uint
    Username string
    FirstName string
    LastName string
}

func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()

    db.CreateTable(&User{})

}

```
When the main function runs, it will do the following:
* create a table called `users`
* create the columns specified in the struct
* convert the camelCase from the struct to snake_case as the column
* it determined that the `ID` field of the type `User` is the primary key constraint
* it'll create the `users_id_seq` in order to determine the next sequence number for that ID

[Back](#table-of-content)
### Creating Records
To create a record

```go
func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()

    db.DropTable(&User{}) // not recommended
    db.CreateTable(&User{})

    user := User {
        Username: "ade",
        FirstName: "Arthur",
        LastName: "Dent",
    }

    db.Create(&user)

}
```
[Back](#table-of-content)
### Querying for Records
To get a record from the database

```go
var users []User = []User{
    User{UserName:"adent", FirstName: "Arthur", LastName: "Dent" },
    User{UserName:"fprefect", FirstName: "Ford", LastName: "Prefect" },
    User{UserName:"tmacmillan", FirstName: "Tricia", LastName: "MacMillan" },
    User{UserName:"mrobot", FirstName: "Marvin", LastName: "Robot" },
}

func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()

    db.DropTable(&User{}) // not recommended
    db.CreateTable(&User{})

    for _, user := range users {
        db.Create(&user)
    }

    u := User{}
    db.First(&u) // Query the first entry in the users table
    db.Last(&u) // Query the last entry of the user table
}
```

[Back](#table-of-content)

### Updating Records

```go
:
func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()

    db.DropTable(&User{}) // not recommended
    db.CreateTable(&User{})

    for _, user := range users {
        db.Create(&user)
    }

    u := User{ Username: "tmacmillan" }
    db.Where(&u).First(&u)
    u.LastName = "Beeblebrox"
    db.Save(&u)

    user := User{}
    db.Where(&u).First(&user) // Find the user with Username == "tmacmillan" and set its value to the user variable

    fmt.Println(user)

}
```
[Back](#table-of-content)

### Deleting Records
```go
:
func main() {
    db, err := gorm.Open("postgres", "user=[username] password=[password] dbname=[database name] sslmode=[disable|enable]")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    dbase := db.DB()
    defer dbase.Close()

    db.DropTable(&User{}) // not recommended
    db.CreateTable(&User{})

    for _, user := range users {
        db.Create(&user)
    }

    db.Where(&User{ Username: "adent"}).Delete(&User{})

```

[Back](#table-of-content)

## Defining Schemas

### Defining Entities

[Back](#table-of-content)


### Scoping
### Defining Tables
### Basic Entity Creation
### Customizing Field Types and Sizes
### Auto-incrementing fields
### Transient fields
### Unique fields
### Preventing Nulls and Providing Default Values
### Primary Key fields
### Controlling Columns Names
### Embedding Child objects
### Working with Indexes

## Working with Relationships
### One-to-One relationships
### Foreign key contraints
### One-to-many relationships
### Many-to-many relationships
### Polymorphism
### Association API

## Creating, Updating, and Deleting Records
### Creating Records
### Creating Records with Children
### Updating Records
### Updating Records with Children
### Batch Updates
### Deleting Records
### Transactions

## Querying the Database
### Retrieving Single Records
### Retrieving Record Sets
### Where Clause
### Preloading Child Object
### Limits, Offsets, and Ordering Records
### Selecting Data subsets
### Using Attrs and Assign to Provide Default values
### Creating projections with Joins
### Working with Raw Result Rows
### Creating Aggregations with Group and Having
### Using Raw SQL


## Modifying Schema
### Creating new entities
### Adding fields to an Entity
### Adding indexes
### Removing Entities
### Modifying a field's type
### Removing a field

## Advance topic
### Callbacks
### Scopes
### Customizing the Logger
